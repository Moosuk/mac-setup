# zsh
https://gist.github.com/kevin-smets/8568070
# vim
https://github.com/amix/vimrc


# vimrc
PlugInstall
ctrl-p for fzf
ctrl-n for nerdtree``
